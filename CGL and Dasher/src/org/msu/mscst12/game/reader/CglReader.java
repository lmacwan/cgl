package org.msu.mscst12.game.reader;
import org.msu.mscst12.game.model.*;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class CglReader {

    private String filePath;
    private boolean initialized;
    private boolean hasErrors;
    private int rowCount;
    private int colCount;
    List<Location> aliveLocations;

    private CglReader(String path) {
        this.filePath = path;
    }

    private CglReader() {
    }

    public static CglReader getInstance(String path) {
        CglReader reader = new CglReader(path);
        reader.hasErrors = true;
        reader.initialized = false;
        return reader;
    }

    public List<Location> getAliveLocations() {
        return aliveLocations;
    }

    public int getColCount() {
        return colCount;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public int getRowCount() {
        return rowCount;
    }

    public boolean isHasErrors() {
        return hasErrors;
    }

    public String getFilePath() {
        return filePath;
    }

    public boolean read() throws IOException, FileNotFoundException {
        initialized = true;
        boolean isFileSucessfullyRead = false;
        //Read from file

        BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
        String str;
        try {
            str = r.readLine();
            StringTokenizer st = new StringTokenizer(str, ",");
            List<Integer> list = new ArrayList<Integer>();
            while (st.hasMoreTokens()) {
                list.add(Integer.parseInt(st.nextToken()));
            }
            rowCount = list.get(0);
            colCount = list.get(1);
            aliveLocations = new ArrayList<Location>();
            for (int i = 2; i < list.size(); i += 2) {
                aliveLocations.add(new Location(list.get(i), list.get(i + 1)));
            }
            hasErrors = false;
            isFileSucessfullyRead = true;
        } catch (IOException ioe) {
            hasErrors = true;
            throw new IOException("File content can not be interpreted. The valid fomat is rowCountForScreen,colCountForScreen,alivePointXCoordianate,alivePointYCoordianate[,alivePointXCoordianate,alivePointYCoordianate]...");
        } catch (IndexOutOfBoundsException iobex) {
            hasErrors = true;
            throw new IndexOutOfBoundsException("File content can not be interpreted. Invalid format for alivePointXCoordianate,alivePointYCoordianate[,alivePointXCoordianate,alivePointYCoordianate]...");
        } catch (NumberFormatException ex) {
           hasErrors = true;
           throw new NumberFormatException("File content can not be interpreted. Invalid numbers entered. Only integers are allowed.");
        }
        return isFileSucessfullyRead;
    }
}
