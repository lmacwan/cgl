
package org.msu.mscst12.game.app;
import org.msu.mscst12.game.model.*;
import org.msu.mscst12.game.reader.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Screen screen = null;
       // CglReader reader = CglReader.getInstance("C:\\Documents and Settings\\Administrator\\Desktop\\InputFile.txt");
       CglReader reader = CglReader.getInstance("/media/Leon/Repositories/Final/CGL and Dasher/src/Input.txt");
        try {
            reader.read();
            if (!reader.isHasErrors()) {
                screen = new Screen(reader.getRowCount(), reader.getColCount());
                System.out.println("Press Enter for Next Generation and Ctrl+Z to Exit .");
                screen.setInitialPattern(reader.getAliveLocations());
                screen.setShapeOnScreen();
                while (true) {
                    String input = sc.nextLine();
		    if (input.toLowerCase().equals("exit")) {
			System.out.println("Program Terminated.");
			break;
		    }
                    screen.getNextGeneration();
                    screen.setShapeOnScreen();
                }
            } else {
                System.out.println("Invalid file content.");
            }
        } catch (FileNotFoundException fnf) {
            System.out.println(fnf.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }  catch (IndexOutOfBoundsException iobex) {
            System.out.println(iobex.getMessage());
        }  catch (NumberFormatException nex) {
            System.out.println(nex.getMessage());
        }
    }
}
