
package org.msu.mscst12.game.model;import java.util.Iterator;
import java.util.List;
//import java.util.ArrayList;

public class Screen {

    private int rows;
    private int columns;
    private Shape shape;
    private int minX,  maxX,  minY,  maxY;
   // private List<ArrayList<Location>> screen = null;
    
    public Screen(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        shape = new Shape();
        minX = 0;
        maxX = rows;
        minY = 0;
        maxY = columns;
        
 //       screen = new ArrayList<ArrayList<Location>>();
 //       initializeScreen();
    }

/*    private void initializeScreen() {
    	ArrayList<Location> outer;
        if (screen != null) {
             for (int i= minX; i< maxX; i++) { 
             	  outer = new ArrayList<Location>();
		  for (int j= minY; i< maxY; j++) {
		  	outer.add(new Location(i, j));
		  }
		  screen.add(outer);
             }
        }
    }
*/

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public void getNextGeneration() {
        shape.next();
    }

    public void setInitialPattern(List<Location> initialPattern) {
        shape.setInitialPattern(initialPattern);
    }

    private void getBoundsForScreen() {
        if (shape.getCurrentShape().keySet().size() > 0) {
            Iterator<Location> iter = shape.getCurrentShape().keySet().iterator();
            Location currLocInMap = iter.next();
            int minRowIndex = currLocInMap.getY();
            int maxRowIndex = currLocInMap.getY();
            int minColIndex = currLocInMap.getX();
            int maxColIndex = currLocInMap.getX();
            while (iter.hasNext()) {
                currLocInMap = iter.next();
                if (currLocInMap.getY() < minRowIndex) {
                    minRowIndex = currLocInMap.getY();
                }
                if (currLocInMap.getY() > maxRowIndex) {
                    maxRowIndex = currLocInMap.getY();
                }
                if (currLocInMap.getX() < minColIndex) {
                    minColIndex = currLocInMap.getX();
                }
                if (currLocInMap.getX() > maxColIndex) {
                    maxColIndex = currLocInMap.getX();
                }
            }
            if (minX >= minColIndex) {
                maxX -= Math.abs(minColIndex - minX);
                minX = minColIndex;
            }
            if (maxX <= maxColIndex) {
                minX += Math.abs(maxColIndex - maxX);
                maxX = maxColIndex;
            }
            if (minY >= minRowIndex) {
                maxY -= Math.abs(minRowIndex - minY);
                minY = minRowIndex;
            }
            if (maxY <= maxRowIndex) {
                minY += Math.abs(maxRowIndex - maxY);
                maxY = maxRowIndex;
            }
        }
    }

    public void setShapeOnScreen() {
        getBoundsForScreen();

	  System.out.println("Generation : "+shape.getGeneration());
	  System.out.println("-----------------------");
	  System.out.println("");
        for (int i = minX - 1; i <= maxX + 1; i++) {
            for (int j = minY - 1; j <= maxY + 1; j++) {
                Location location = new Location(i, j);
                if (!shape.getCurrentShape().containsKey(location)) {
                    System.out.print("- ");
                } else {
                    System.out.print("X ");
                }
            }
            System.out.println("");
        } //end of outer for
	  System.out.println("");
    } 
}
