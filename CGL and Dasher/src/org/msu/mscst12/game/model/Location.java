package org.msu.mscst12.game.model;

public class Location implements Comparable<Location> {
   private int x;
   private int y;

   public Location (int x, int y) {
	this.x = x;
	this.y = y;
   }

   public int getX() {
      return this.x;
   }

   public int getY() {
      return this.y;
   }

   public int compareTo(Location loc) {
	int xDistance = loc.getX() - this.x ;
        int yDistance = loc.getY() - this.y ;
        if ((xDistance == 0) && (yDistance == 0)) {
            return 0;
        }
        if ((xDistance < 0) || (yDistance < 0)) {
            return -(-Math.abs(xDistance) - Math.abs(yDistance));
        }
        return -(Math.abs(xDistance) + Math.abs(yDistance));
    }

    @Override public boolean equals(Object o) {
	return this.x == ((Location)o).getX() && this.y == ((Location)o).getY();
    }

    @Override public int hashCode() {
	int hash = 7;
    	hash = 71 * hash + this.x;
    	hash = 71 * hash + this.y;
    	return hash;
    }

    public String toString() {
	return "Location : (" + this.x + ", " + this.y +") ";
    }
}

