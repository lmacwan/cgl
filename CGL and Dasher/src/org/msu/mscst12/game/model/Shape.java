package org.msu.mscst12.game.model;
import java.util.*;

public class Shape {

    private boolean initialized;
    private int generation = 0;
    private Map<Location, Object> currentShape = new HashMap<Location, Object>();
    private Map<Location, Object> nextShape = new HashMap<Location, Object>();
    private int rowCount,  colCount,  firstRow,  lastRow,  firstCol,  lastCol;

    public Shape() {
        initialized = false;
    }

    public int getGeneration() {
        return generation;
    }

    public int getRowsCount() {
        return rowCount;
    }

    public int getColumnsCount() {
        return colCount;
    }

    public void setInitialPattern(List<Location> aliveLocations) {
        for (Location loc : aliveLocations) {
            currentShape.put(loc, new Object());
        }
        initialized = true;
    //pattern for Glider
//        currentShape.put(new Location(4, 4), new Object());
//        currentShape.put(new Location(4, 5), new Object());
//        currentShape.put(new Location(4, 6), new Object());
//        currentShape.put(new Location(3, 4), new Object());
//        currentShape.put(new Location(2, 5), new Object());

    //pattern for Oscillator
        /*
    currentShape.put(new Location(7, 6), new Object());
    currentShape.put(new Location(7, 5), new Object());
    currentShape.put(new Location(7, 7), new Object());
    /*

    //pattern for Still Shape

    /*currentShape.put(new Location(6, 7), new Object());
    currentShape.put(new Location(7, 7), new Object());
    currentShape.put(new Location(6, 8), new Object());
    currentShape.put(new Location(7, 8), new Object());
     */
    }

    public Map<Location, Object> getCurrentShape() {
        return currentShape;
    }

    public Map<Location, Object> getNextShape() {
        return nextShape;
    }

    public void next() {
        int aliveCount = 0;
        generation++;
        nextShape.clear();

        //Location[] allLiveCellLocations = currentShape.keySet().toArray(new Location[currentShape.keySet().size()]);
        if (currentShape.keySet().size() > 0) {
            Iterator<Location> iter = currentShape.keySet().iterator();
            Location currLocInMap = iter.next();
            int minRowIndex = currLocInMap.getY();
            int maxRowIndex = currLocInMap.getY();
            int minColIndex = currLocInMap.getX();
            int maxColIndex = currLocInMap.getX();
            while (iter.hasNext()) {
                currLocInMap = iter.next();
                if (currLocInMap.getY() < minRowIndex) {
                    minRowIndex = currLocInMap.getY();
                }
                if (currLocInMap.getY() > maxRowIndex) {
                    maxRowIndex = currLocInMap.getY();
                }
                if (currLocInMap.getX() < minColIndex) {
                    minColIndex = currLocInMap.getX();
                }
                if (currLocInMap.getX() > maxColIndex) {
                    maxColIndex = currLocInMap.getX();
                }
            }

            for (int i = minRowIndex-1; i <= maxRowIndex+1; i++) {
                for (int j = minColIndex-1; j <= maxColIndex+1; j++) {
                    Location l = new Location(j, i);
                    aliveCount = new Cell(l).getAliveCount(currentShape);
                    if (currentShape.containsKey(l)) {
                        if (aliveCount == 2 || aliveCount == 3) {
                            nextShape.put(l, new Object());
                        }
                    } else {
                        if (aliveCount == 3) {
                            nextShape.put(l, new Object());
                        }
                    }
                }
            }
        }
        currentShape.clear();
        currentShape.putAll(nextShape);
    }
}
