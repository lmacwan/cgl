
package org.msu.mscst12.game.model;

import java.util.*;

public class Cell {
    public enum State {
        ALIVE,
        DEAD,
    }    
    public enum Neighbour {
        TOP_LEFT(-1, -1) {
	    public String toString() {
		return "TOPLEFT";
	    }
	},
        TOP_CENTER(-1, 0){
	    public String toString() {
		return "TOPCENTER";
	    }
	},
        TOP_RIGHT(-1, 1){
	    public String toString() {
		return "TOPRIGHT";
	    }
	},
        CENTER_LEFT(0, -1){
	    public String toString() {
		return "CENTERLEFT";
	    }
	},
        CENTER_RIGHT(0, 1){
	    public String toString() {
		return "CENTERRIGHT";
	    }
	},
        BOTTOM_LEFT(1, -1){
	    public String toString() {
		return "BOTTOMLEFT";
	    }
	},
        BOTTOM_CENTER(1, 0){
	    public String toString() {
		return "BOTTOMCENTER";
	    }
	},
        BOTTOM_RIGHT(1, 1){
	    public String toString() {
		return "BOTTOMRIGHT";
	    }
	},
        ;
        private Neighbour(int xo, int yo) {
	    this.xOffset = xo;
	    this.yOffset = yo;
        }   
        private int xOffset;
	private int yOffset; 
	public Location getLocationWRT(Location loc) {
	   return new Location(loc.getX() + xOffset, loc.getY() + yOffset);
	}
	
    }
    private Location location;	
    private State state = State.DEAD;

    public Cell(Location loc) {
	if (loc != null) {
	    this.location = loc;
	}
    }

    public void setState(State s) {
	this.state = s;
    }

    public State getState() {
	return this.state;
    }      

    public Location getLocation() {
	return this.location;
    }

    public String toString() {
	return "Cell Location : (" + this.location.getX() + ", " + this.location.getY() + ")";
    }
   
    public int getAliveCount(Map<Location, Object> currentShape) {
	int count = 0;
	for (Neighbour n : Neighbour.values()) {
	    //System.out.println(n.getLocationWRT(this.location));
	    if (currentShape.containsKey(n.getLocationWRT(this.location))) {
		count++;
	    }
	}
	
	/*if (count > 0) {	
	    System.out.println("State and Live Neighbour count of the cell : ");
	    System.out.println(this.location + " ,  State : " +this.getState() + ", AliveNeighbourCount : " +count);
	}*/
	return count;
    }

    public int getDeadCount(Map<Location, Object> currentShape) {
	int count = 0;
	for (Neighbour n : Neighbour.values()) {
	    if (!currentShape.containsKey(n.getLocationWRT(this.location))) {
		       //getInstance().getCellState(n.getLocationWRT(this.location)) == Cell.State.DEAD) {
		count++;
	    }
	}	
	return count;
    }            
}

