package org.msu.mscst12.game.gui;
import org.msu.mscst12.game.model.*;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;


public class CglGui extends JFrame {

   private JTable jtable;
   private MyTableModel tableModel;
   
   public CglGui() {
   	Shape shape = new Shape();
   	tableModel = new MyTableModel(shape);
   	jtable = new JTable(tableModel);
   	JScrollPane scrollPane = new JScrollPane(jtable);
   	getContentPane().add(scrollPane);
   }
   
   public static void main(String[] args) {
   	CglGui cglGui = new CglGui();
   	cglGui.setVisible(true);
   	cglGui.setSize(new Dimension(700, 700));
   	cglGui.validate();
   }
}

class MyTableModel extends AbstractTableModel {

	private Shape shape;
	public MyTableModel(Shape board) {
		super();
		this.shape = board;
	}
	
	public int getColumnCount() {
		return shape.getRowsCount();
	}

	public int getRowCount() {
		return shape.getColumnsCount();
	}
	
	public Object getValueAt(int row, int col) {
		return (shape.getCurrentShape().get(new Location(col, row)) == null ? false : true);
	}
}
