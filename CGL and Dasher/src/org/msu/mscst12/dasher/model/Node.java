package org.msu.mscst12.dasher.model;

import java.util.Set;
import java.util.TreeSet;
import org.msu.mscst12.dasher.model.Tree;

public class Node implements Comparable<Node> {
    private Character data = null;
    private Node parent = null;
    private Set<Node> childNodes = new TreeSet<Node>();
    private boolean isWord = false;    

    private Node() { };
    
    public static Node getInstance(Character data) {
        Node node = new Node();
        node.data = data;
        return node;
    }
    
    public void setIsWord(boolean isWord) {
    	this.isWord = isWord;
    }
    
    public boolean isWord() {
        return this.isWord;
    }

    public void setData(Character data) {
    	this.data = data;
    }
    
    public Character getData() {
        return this.data;
    }
    
    public void setParent(Node parent) {
        this.parent = parent;
    }
    
    public Node getParent() {
        return this.parent;
    }
    
    public Set<Node> getChildren() {
        return childNodes;
    }
    
    public boolean hasChildren() {
        return !(childNodes.isEmpty());
    }

    @Override
    public int compareTo(Node n) {
	return ((int)(Character.toLowerCase(this.data))) - ((int)(Character.toLowerCase(n.getData())));
    }

    @Override
    public boolean equals(Object n) {
	if (this == n) return true;
	if (!(n instanceof Node)) return false;
	return Node.getInstance(Character.toLowerCase(((Node)n).getData())).getData().charValue() == Node.getInstance(Character.toLowerCase(this.data)).getData();
    }

    @Override
    public int hashCode() {
	return this.data == null ? -1 : ((int)(Character.toLowerCase(this.data)));
    }
  
} 
