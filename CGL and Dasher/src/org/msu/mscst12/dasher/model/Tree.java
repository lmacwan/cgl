package org.msu.mscst12.dasher.model;

import java.util.*;
import org.msu.mscst12.dasher.model.Node;

public class Tree {
     private Tree() {} ;
     private Node rootNode = null;

     /**
     *This field holds data about the depth of the tree, which is initially set to zero (0).
     */
     private int depth = 0;
    

     /**
     *The static factory method to obtain an initial tree which only contains the root.
     */
     public static Tree getInstance() {
         Tree tree = new Tree();
         Node rootNode = Node.getInstance(null);
         rootNode.setParent(null);
	 tree.rootNode = rootNode;
	 return tree;        
     }
     
     /**
     * Returns the root of this tree.
     */
     public Node getRootNode() {
         return rootNode;
     }     
     
     /**
     * The method adds a given node to the tree setting 
     * its parent to an already exisiting node of the tree. 
     * The node to be added is specified by the node parameter.
     * The parent is specified by the parent parameter.
     */    
     private boolean addNode(Node node, Node parent) {
         if (node != null && parent != null) {
             
 	     if (parent.getChildren().add(node)) {
		node.setParent(parent);
		//System.out.println("Parent set");
                //int levelOfNode = getLevel(node);
                //depth = (depth > levelOfNode) ? levelOfNode : depth;
		//System.out.println("Depth Set.");	
		return true;  
	     }
	     else {
		System.out.println("Node Not Added.");
	     }
         }
         return false;
     }
 

     /**
     * The method tries to recursively add nodes supplied as list.
     * The next node in sequence is added as a child to the current node, 
     * where as the first node in the list is the child of teh supplied initial parent.
     * If some node cannot be added because it has been already added,
     * that node is skipped and the next node in sequence is added as its child.
     */
     private Node tryAddingNodes(List<Node> nodesToAdd, Node initialParent) {
	Node parent = null;
	if (!nodesToAdd.isEmpty()) {
	    if (this.addNode(nodesToAdd.get(0), initialParent)) {
		System.out.println(nodesToAdd.get(0).getData().charValue()+" Added.");
		parent = nodesToAdd.get(0);
	        nodesToAdd.remove(0);
	    } else {
		System.out.println("Entered Else");
		for (Node n : initialParent.getChildren()) {
		    if (n == nodesToAdd) {
			parent = n;
			break;
		    }
		}
	    }
	    tryAddingNodes(nodesToAdd, parent);
	} 
	return parent;
     }


     /**
     * The method add the characters of the word to the tree in sequence, 
     * setting the isWord attribute to true on node corresponding to the last character of the word.
     */
     public boolean addWord(String word) {
	 if (!word.isEmpty()) {
	    Node lastNode = tryAddingNodes(getNodesList(word), rootNode);
	    lastNode.setIsWord(true);
	 }
	 return false;
     }


     
     private List<Node> getNodesList(String word) {
	 List<Node> nodes = new ArrayList<Node>();
	 for (char c : word.toLowerCase().toCharArray()) {
	     nodes.add(Node.getInstance(c));
 	 }
	 System.out.println("Nodes Generated : "+(!nodes.isEmpty())+" Root Node : "+rootNode);
	 return nodes; 
     }

     private int getLevel(Node parent) {
         //System.out.println("Get Level Entered");
	 //System.out.println(parent.hasChildren());
	 int level = 0;
         while(parent.getParent() != null) {
             level++;
         }
	 System.out.println("Level : "+level);
         return level;
     }

/*
     public Set<Node> getFirstLevelNodes() {
         return rootNode.getChildren();
     }
     
     public Set<Node> getNthLevelNodes(int n) {
         Set<Node> nodesAtGivenLevel = new TreeSet<Node>();
         int level = 0;
         Node currNode = this.rootNode;
         Set<Node> intermediateNodes = new TreeSet<Nodes>();
         if (n >= 1) {
             
             while (currNode.getChildren().isEmpty()) {
                 level++;
                 if (level == n) {
                 
                 } else {
                     for (Node n : currNode.getChildren()) {
                         
                     }
                 }
             } 
         }
         
     }   
*/   
}
