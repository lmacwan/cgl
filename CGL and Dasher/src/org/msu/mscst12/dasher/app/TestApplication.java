package org.msu.mscst12.dasher.app;

import java.util.*;
import org.msu.mscst12.dasher.model.*;

public class TestApplication {

   public static void main(String[] args) {
	Tree tree = Tree.getInstance();

	tree.addWord("Leon");
	tree.addWord("Leena");
	//System.out.println(tree.getRootNode());
	//System.out.println(tree.getRootNode().getChildren());
	getTreeString(tree.getRootNode());
   }

   public static void getTreeString(Node node) {
	if (node.getData() == null) {
	    System.out.println("Root Node");
	} else {
	     System.out.print("Node : "+node.getData().charValue());
	}
	if (!node.getChildren().isEmpty()) {
	    System.out.print(" => Children : ");
	    for (Node n : node.getChildren()) {
		System.out.print(n.getData().charValue()+" | ");
		if (!n.getChildren().isEmpty()) {
		     System.out.println("");
		     getTreeString(n);
		}
	    }
	} 
   }
}